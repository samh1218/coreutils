#!/user/bin/env python

from setuptools import setup, find_packages

setup(name='coreutils',
        version='1.0',
        description='Core Text Preprocessing Library',
        author='Samuel Huh',
        author_email='samh1218@gmail.com',
        url='https://samh1218@bitbucket.org/samh1218/coreutils.git',
        packages=find_packages(),
        include_package_data=True,
        zip_safe=False,
        test_suite="nose.collector",
        tests_require=['nose'],
        install_requires=[
            'nltk',
            'pandas',
            'tensorflow',
            'nose'
        ]
        )

