

"""
 All Text Distance Functions.

 @author Samuel Huh
"""

class Distance(object):
    def damerauLevenshtein(self, seq1, seq2):
        """
         Generalized Damerau Levenshtein (D-L) distance.
         Instead of the classic 3 operations to compute 'cost'
         D-L distance takes into account transposition of two adjacent characters.

         This will make the D-L perform better in capturing distance
         that must take into account the location and sequence of characters.
            @param A seq1
            @param B seq2
            @return the distance between A and B. The lower the distance, the better.
        """
        one_ago = None
        this_row = list(range(1, len(seq2) + 1)) + [0]
        for x in range(len(seq1)):
            two_ago, one_ago, this_row = one_ago, this_row, [0] * len(seq2) + [x + 1]
            for y in range(len(seq2)):
                del_cost = one_ago[y] + 1
                add_cost = this_row[y - 1] + 1
                sub_cost = one_ago[y-1] + (seq1[x] != seq2[y])
                this_row[y] = min(del_cost, add_cost, sub_cost)
                if (x > 0 and y > 0 and seq1[x] == seq2[y-1]
                        and seq1[x-1] == seq2[y] and seq1[x] != seq2[y]):
                    this_row[y] = min(this_row[y], two_ago[y-2] + 1)
        return this_row[len(seq2) - 1]
