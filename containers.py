
"""
 This script contains all the custom text classes for
 storage, text manipulation, and consistent input/output file types
 
 Supported Format :
 1. { Term Term Term|tag} term term term
 2. term/tag term/tag term/tag
 
 author Samuel Huh
"""

from coreutils.standardizer import Standardizer

def dismantle(text):
    words = text.split(" ")
    term = None
    tag = None
    if words is None:
        words = []
    for word in words:
        wordAndTag = word.split("|")
        if len(wordAndTag) == 2:
            tag = Standardizer().cleanUp(wordAndTag[1])
        term = Standardizer().cleanUp(wordAndTag[0])
    if term is None:
        return text,tag
    return term,tag

class Sentence:
    tokens = None
    entityCount = 0
    text = None
    wordsOnly = None

    def __init__(self, text=""):
        self.tokens = []
        self.textOnly=""
        self.text = ""
        text = text.strip()
        if not text:
            return
        words = text.split(" ")
        for word in words:
            term,tag = dismantle(word)
            token = Token(term,tag)
            if token.isEntity():
                self.entityCount +=1
            self.tokens.append(token)
        self.updateText()

    def toGroup(self):
        if len(self.tokens) < 2:
            return self.tokens
        groupedTokens = []
        newToken = Token(self.tokens[0].text, self.tokens[0].tag)
        i = 1
        while (i < len(self.tokens)):
            if self.tokens[i].tag == newToken.tag:
                newToken.addWord(self.tokens[i].text)
            else:
                groupedTokens.append(newToken)
                newToken = Token(self.tokens[i].text, self.tokens[i].tag)
            i+=1
        if newToken is not None:
            groupedTokens.append(newToken)
        return groupedTokens

    def addToken(self, token):
        if (token is None):
            return
        if (not token.Text and not token.tag):
            return
        if (token.isEntity()):
            self.entityCount+=1
        self.tokens.append(token)
        self.updateText()

    def getToken(self, index):
        if (index < 0 or index >= len(self.tokens)):
            return None
        return self.tokens[index]
 
    def setToken(self, index, token):
        if (index < 0 or index >= len(self.tokens)):
            return
        if (self.tokens[index].isEntity()):
            self.entityCount-=1
        self.tokens[index]=token
        if (token.isEntity()):
            self.entityCount+=1
        self.updateText()

    def group(self):
        self.tokens = self.toGroup()
        self.entityCount = 0
        for token in self.tokens:
            if token.isEntity():
                self.entityCount+=1
        self.updateText()

    def textOnly(self):
        return self.wordsOnly

    def toString(self):
        return self.text

    def size(self):
        return len(self.tokens)

    def updateText(self):
        newText = []
        wordsOnly = []
        for token in self.tokens:
            newText.append(token.toString())
            wordsOnly.append(token.textOnly())
        self.text = " ".join(newText)
        self.wordsOnly = " ".join(wordsOnly)

class Token:
    text = None
    tag = None
    size = 0

    def __init__(self, text="", tag=None):
        if (text == ""):
            self.size = 0
        else:
            words = text.split(" ")
            self.size = len(words)
        self.text = text
        self.tag = tag

    def addWord(self,word):
        if word is not None:
            self.text += " " + word
            self.text = self.text.strip()
            self.size = len(self.text.split(" "))

    def length(self):
        return self.size

    def setTag(self, tag):
        self.tag = tag

    def textOnly(self):
        return self.text

    def getTag(self):
        return self.tag

    def toString(self):
        if self.isEntity() is not True:
            return self.text
        result = []
        result.append("{")
        result.append(self.text)
        result.append("|")
        result.append(self.tag)
        result.append("}")
        return "".join(result)

    def isEntity(self):
        return self.tag is not None and self.tag != "O"
