
"""
 Author : Samuel Huh

 This is a singleton class.
 This class is responsible for cleaning up the text
 prior to any machine learning work.
"""

import re
import os
from nltk.stem import WordNetLemmatizer

DEFAULT_JAR = ""

class Standardizer:
    class _Standardizer:
        lemmatizer = None
        stopwords = None

        def __init__(self):
            self.stopwords = []

        def _lemmatize(self, text):
            if len(DEFAULT_JAR) == 0:
                lemmatizer = WordNetLemmatizer()
                return self.lemmatizer.lemmatize(text)
            else:
                inFile = open("temp.txt", "w+")
                inFile.write(text)
                inFile.close()
                # Use Subprocess to call the JAR for lemmatization
                subprocess.call(['java', '-Xmx16g', '-Xms16g', '-cp', DEFAULT_JAR, 'Main', 'lemmatize', "temp.txt", "temp-annotate.txt"])
                result = open("temp-annotate.txt", "r").readlines()
                os.remove("temp.txt")
                os.remove("temp-annotate.txt")
                return result
        def _removeDuplicateSpaces(self, text):
            pattern = re.compile(r" +")
            return re.sub(pattern, " ", text)

        def _removeSpecialChars(self, text):
            replaceWithVoids  = re.compile(r"[^a-zA-Z0-9\/_\.]", re.IGNORECASE)
            replaceWithSpaces = re.compile(r"[\/_]")
            dashRegex = re.compile(r"-+")
            periodRegex = re.compile(r"\\.+")
            text = text.rstrip()
            text = text.strip()
            text = re.sub(replaceWithVoids, "", text)
            text = re.sub("{", "", text)
            text = re.sub(replaceWithSpaces, " ", text)
            text = re.sub(dashRegex, "-", text)
            return text

        def _removeStopwords(self, text):
            words = text.split(" ")
            if (len(words) > 1):
                newText = []
                for word in words:
                    if word not in self.stopwords:
                        newText.append(word)
                return " ".join(newText)
            return text

        def _removeNonAscii(self, text):
            return re.sub(r'[^\x00-\x7F]+', ' ', text)

        def _cleanUp(self, text):
            text = self._removeNonAscii(text)
            text = self._removeDuplicateSpaces(text)
            text = self._removeSpecialChars(text)
            text = self._removeStopwords(text)
            return text

        def _normalize(self, text):
            text = self._cleanUp(text)
            text = self._lemmatize(text)
            return text

        def _normalizeFile(self, inPath, outPath):
            inFile  = open(inPath, "r")
            inLines = inFile.readlines()
            outFile = open(outPath, "w+")
            for line in inLines:
                normalized = self._normalize(line)
                outFile.write(normalized + "\n")

    instance = None

    def __init__(self):
        self.instance = self._Standardizer()

    def lemmatize(self, text):
        return self.instance._lemmatize(text)

    # Clean data without lemmatization
    def cleanUp(self, text):
        return self.instance._cleanUp(text)

    def removeDuplicateSpaces(self, text):
        return self.instance._removeDuplicateSpaces(text)

    def removeStopwords(self, text):
        return self.instance._removeStopwords(text)

    # 'cleanUp' + lemmatization
    def normalize(self, text):
        return self.instance._normalize(text)

    def normalizeFile(self, inPath, outPath):
        self.instance._normalizeFile(inPath, outPath)

    def removeNonAscii(self, text):
        return self.instance._removeNonAscii(self, text)

