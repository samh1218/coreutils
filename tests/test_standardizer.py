
import unittest

from coreutils.standardizer import Standardizer

class TestStandardizer(unittest.TestCase):
    def test_removeDuplicateSpaces(self):
        text = ""
        text = Standardizer().removeDuplicateSpaces(text)
        self.assertEqual(text, "")
        text = "  "
        text = Standardizer().removeDuplicateSpaces(text)
        self.assertEqual(text, " ")
        text = "     "
        text = Standardizer().removeDuplicateSpaces(text)
        self.assertEqual(text, " ")
        text = "  aa  bb"
        text = Standardizer().removeDuplicateSpaces(text)
        self.assertEqual(text, " aa bb")

if __name__ == '__main__':
    unittest.main()
