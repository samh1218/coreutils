
import unittest

from coreutils.containers import Sentence, Token

class TestSentence(unittest.TestCase):
    def test_constructor(self):
        text1 = "hello world"
        sentence1 = Sentence(text1)
        self.assertEqual(sentence1.size(), 2)
        self.assertEqual(sentence1.entityCount, 0)
        self.assertEqual(sentence1.text, text1)
        text2 = "{hello|w}"
        sentence2 = Sentence(text2)
        self.assertEqual(sentence2.size(), 1)
        self.assertEqual(sentence2.entityCount, 1)
        self.assertEqual(sentence2.getToken(0).text, "hello")
        text3 = "my name is {Sam|person}. What is your name?"
        sentence2 = Sentence(text3)
        self.assertEqual(sentence2.size(), 8)
        self.assertEqual(sentence2.entityCount, 1)
        self.assertEqual(sentence2.wordsOnly, "my name is Sam What is your name")

        text1 = "               "
        sentence1 = Sentence(text1)
        self.assertEqual(sentence1.size(), 0)
        self.assertEqual(sentence1.entityCount, 0)
        text1 = ""
        sentenc1 = Sentence(text1)
        self.assertEqual(len(sentence1.tokens), 0)
        self.assertEqual(sentence1.entityCount, 0)

    def test_toGroup(self):
        text = "{hello|w} {world|w}"
        sentence = Sentence(text)
        self.assertEqual(sentence.size(), 2)
        self.assertEqual(sentence.entityCount, 2)
        self.assertEqual(sentence.getToken(0).text, "hello")
        self.assertEqual(sentence.getToken(1).text, "world")
        sentence.group()
        self.assertEqual(sentence.size(), 1)
        self.assertEqual(sentence.entityCount, 1)
        self.assertEqual(sentence.getToken(0).text, "hello world")

    def addToken(self):
        token = Token("sam", None)
        sentence = Sentence("{hello|v} world")
        sentence.addToken(token)
        self.assertEqual(sentence.toString(), "{hello|v} world sam")

if __name__ == '__main__':
    unittest.main()
