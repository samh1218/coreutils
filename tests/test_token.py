
import unittest
from coreutils.containers import Token

class TestToken(unittest.TestCase):
    def test_constructor(self):
        token = Token("hello")
        self.assertEqual(token.text, "hello")
        self.assertFalse(token.tag)
        token = Token("hello", "sw")
        self.assertEqual(token.text, "hello")
        self.assertEqual(token.tag, "sw")

    def test_length(self):
        token = Token("hello")
        self.assertEqual(token.size, 1)
        token = Token("hello world")
        self.assertEqual(token.length(), 2)

    def test_addWord(self):
        token = Token()
        self.assertEqual(token.length(), 0)
        self.assertEqual(token.toString(), "")
        token.addWord("hello")
        self.assertEqual(token.length(), 1)
        self.assertEqual(token.toString(), "hello")
        token.addWord("word man")
        self.assertEqual(token.length(), 3)
        self.assertEqual(token.toString(), "hello word man")

if __name__ == '__main__':
    unittest.main()
