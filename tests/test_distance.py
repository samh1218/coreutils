
import unittest
from coreutils.distance import Distance

class TestDistance(unittest.TestCase):
    def test_distance_zero(self):
        dist = Distance()
        strA = 'Hello'
        strB = 'Hello'
        expected = 0
        result = dist.damerauLevenshtein(strA, strB)
        self.assertEqual(expected, result)
    def test_distance_non_zero(self):
        dist = Distance()
        strA = 'Hello'
        strB = 'World'
        expected = 4
        result = dist.damerauLevenshtein(strA, strB)
        self.assertEqual(expected, result)
    def test_distance_case_insensitive(self):
        dist = Distance()
        strA = 'Hello'
        strB = 'HELLO'
        expected = 4
        result = dist.damerauLevenshtein(strA, strB)
        self.assertEqual(result, expected)

if __name__ == '__main__':
    unittest.main()
