# coreutils : main text preprocessing library to suit all your needs!

This is the core text utility library used throughout any of my side projects.

## This library contains :

1. Custom Containers : For text manipulation and compatibility with different input/output file types.
	1. Token : represents a single term in a sentence.
	2. Sentence : represents a collection of tokens (a.k.a. a single sentence).
2. Text Standardizer : For data cleaning and preprocessing of texts.
3. SynomymLibrary : used for finding the entity name and storing synonyms of that entity.
4. Distance : To calculate the text distance between two texts.
5. Token Splicing : To add the correct space in a token missing a space.

Uses custom CRF Classifier for NER and custom lemmatizer and PoS Tagger for these operations.
For the sake of space, those two classifers are NOT added in this repository.