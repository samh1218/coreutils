
import sys
from coreutils.standardizer import Standardizer
from coreutils.containers import Sentence, Token
from coreutils.distance import Distance

SCORE_THRESHOLD = 4

class SynonymLibrary:
    class _SynonymLibrary:
        _synonymDictionary = None
        def __init__(self):
            _synonymDictionary = {}

        def _updateSubTable(self, tableKey, synonym, annotation):
            subtable = {}
            if (tableKey in synonymDictionary):
                subtable = synonymDictionary[tableKey]
            subtable[synonym] = annotation
            synonymDictionary[tableKey] = subtable

        def _matchWithSynonym(self, subtable, token):
            text = Standardizer().normalize(token.text)
            annotatedToken = Token(text, None)
            if (subtable):
                min_cost = sys.float_info.min
                synonyms = subtable.keys()
                synonyms.sort(lambda x, y: len(x) > len(y))
                for synonym in synonyms:
                    if (synonym == text):
                        annotatedToken.setTag(subtable[synonym])
                        return annotatedToken
                    cost = Distance().damerauLevenshtein(text, synonym)
                    if (cost < min_cost):
                        min_cost = cost
                        annotatedToken.setTag(subtable[synonym])
                if (min_cost <= SCORE_THRESHOLD):
                    return annotatedToken
            return token

        def _annotate(sentence):
            annotatedSentence = Sentence()
            for token in sentence.getTokens():
                if token.tag in synonymDictionary:
                    token = matchWithSynonym(synonymDictionary[token.tag], token)
                annotatedSentence.addToken(token)
            return annotatedSentence

    instance = None
    def __init__(self):
        instance = _SynonymLibrary()

    def updateSubTable(self, tableKey, synonym, annotation):
        instance._updateSubTable(tableKey, synonym, annotation)
    
    def matchWithSynonym(self, subtable, token):
        return instance._matchWithSynonym(subtable, token)

    def annotate(self, sentence):
        return instance._annotate(sentence)

